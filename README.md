# JavaScript Task 1 - PC Shop

Basic JavaScript application for a laptop shop. The user can work to make money, transfer their salary to their bank, take a loan based on certain conditions, and choose between different laptops to buy.