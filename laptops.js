let laptops = [
    { pc: {
        name: "Dell XPS 15 9500 15,6' UHD+ touch",
        price: '45990',
        description: 'The Dell XPS 15 Might Be the Best All-Around Laptop You Can Buy',
        features: {
            screen: "15,6' UHD+ touch",
            processor: 'Core i9-10885H',
            memory: '64 GB',
            storage: '2 TB SSD',
            OS: 'Windows 10 Home',
            graphics: 'GeForce GTX 1650 Ti'
        },
        image: 'https://www.komplett.no/img/p/1080/1159638.jpg'
    }},
    { pc: {
        name: "ASUS Chromebook 12 C223NA 11,6' HD",
        price: '1990',
        description: 'Extremely cheap!!',
        features: {
            screen: "11,6' HD",
            processor: 'Intel Celeron N3350',
            memory: '4 GB',
            storage: '32 GB SSD',
            OS: 'Google Chrome OS',
            graphics: 'Intel HD Graphics 500'
        },
        image: 'https://www.komplett.no/img/p/1620/1155294.jpg'
    }},
    { pc: {
        name: "MacBook Pro 13 (2020) 1TB Space Gray",
        price: "27490",
        description: "A little overpriced",
        features: {
            screen: "13,3' - IPS",
            processor: 'Intel Core i5 (10. generation) 2 GHz',
            memory: '16 GB',
            storage: '1 TB SSD',
            OS: 'Apple macOS Catalina 10.15',
            graphics: 'Intel Iris Plus Graphics'
        },
        image: "https://www.komplett.no/img/p/1200/1158879.jpg"
    }},
    { pc: {
        name: "Microsoft Surface Pro 7 12,3' touch Platinum",
        price: 9190,
        description: "You can detatch the screen!",
        features: {
            screen: "12,3' touch",
            processor: "Core i3-1005G1",
            memory: "4 GB",
            storage: "128 GB SSD",
            OS: "Windows 10 Home",
            graphics: "Intel UHD Graphics"
        },
        image: "https://www.komplett.no/img/p/1200/1139288.jpg"
    }}
]

export default laptops;