import laptops from './laptops.js';

let pay = document.getElementById("pay");
let balance = document.getElementById("balance");
let featureList = document.getElementById("featureList");
pay.innerHTML = 0;
balance.innerHTML = 0;

let btnWork = document.getElementById("btnWork");
let btnBank = document.getElementById("btnBank");
let btnGetLoan = document.getElementById("btnGetLoan");
let btnBuy = document.getElementById("btnBuy");
btnWork.onclick = () => work();
btnBank.onclick = () => bank();
btnGetLoan.onclick = () => getLoan();
btnBuy.onclick = () => buyLaptop();

function work(){
    const increaseFactor = 100;
    let oldSalary = parseInt(pay.innerHTML);
    let newSalary = parseInt(oldSalary+increaseFactor);
    pay.innerHTML = newSalary;
}

//Move salary to bank account
function bank(){
    let oldBalance = parseInt(balance.innerHTML);
    let salary = parseInt(pay.innerHTML);
    balance.innerHTML = parseInt(oldBalance + salary);
    pay.innerHTML = 0;
}

let hasGottenLoanBefore = false;
function getLoan(){
    if(hasGottenLoanBefore){
        alert("You need to buy a computer before getting a new loan");
        return;
    }

    let amount = prompt("Please enter amount");

    if(amount.match(/^[0-9]+$/) == null){
        alert("Your amount can only contain numbers");
        return;
    }
    if(parseInt(amount) > parseInt(balance.innerHTML)*2){
        alert("You can not loan more than double your bank balance");
        return;
    }
    let oldBalance = parseInt(balance.innerHTML);
    let loan = parseInt(amount);
    balance.innerHTML = parseInt(oldBalance + loan);
    hasGottenLoanBefore = true;
    alert("You got a loan of "+loan+"kr!");
}

let laptopsDropdown = document.getElementById('laptopsDropdown');
laptopsDropdown.length = 0;


//Fill the dropdown with laptop names
function fillDropdown(){
    for(let i = 0; i < laptops.length; i++){
        let option = document.createElement('option');
        option.text = laptops[i].pc.name;
        laptopsDropdown.add(option)
    }
}

let outpName = document.getElementById("outpName");
let outpDescription = document.getElementById("outpDescription");
let outpPrice = document.getElementById("outpPrice");
let imgLaptop = document.getElementById("imgLaptop");

laptopsDropdown.onchange = () => changeLaptopView();

function changeLaptopView(){
    var selectedLaptop = laptopsDropdown.options[laptopsDropdown.selectedIndex];

    //Find selected laptop and diplay it
    for (let i = 0; i < laptops.length; i++){
        if(laptops[i].pc.name == selectedLaptop.value){
            featureList.innerHTML = "";
            outpName.innerHTML = laptops[i].pc.name;
            outpDescription.innerHTML = laptops[i].pc.description;
            outpPrice.innerHTML = laptops[i].pc.price + " kr";
            imgLaptop.src = laptops[i].pc.image;
            fillFeatureList();
        }
    }
}

//Retrieves the features of the currently selected laptop and lists them
function fillFeatureList(){
    var selectedLaptop = laptopsDropdown.options[laptopsDropdown.selectedIndex];
    for (var i = 0; i < laptops.length; i++){
        if(laptops[i].pc.name == selectedLaptop.value){
            let laptop = laptops[i];

            //For every feature, create a new list item displaying it
            Object.keys(laptop.pc.features).forEach(key => {
                const item = document.createElement('li');
                item.innerText = key + ": " + laptop.pc.features[key];
                featureList.appendChild( item );
            })  
        }
    }
}

function buyLaptop(){
    let selectedLaptop = laptopsDropdown.options[laptopsDropdown.selectedIndex];
    let selectedPrice = 0;
    let selectedName = null;

    //Find selected laptops price
    for (let i = 0; i < laptops.length; i++){
        if(laptops[i].pc.name == selectedLaptop.value){
            selectedPrice = parseInt(laptops[i].pc.price);
            selectedName = laptops[i].pc.name;
        }
    }
    if(parseInt(balance.innerHTML) >= selectedPrice){
        balance.innerHTML = parseInt(balance.innerHTML - selectedPrice);
        hasGottenLoanBefore = false;
        alert("Congratulations! you just bought " + selectedName);
    }
    else{
        alert("Sorry, you can not afford this computer, keep working or take a loan!");
    }

}

outpName.innerHTML = laptops[0].pc.name;
outpDescription.innerHTML = laptops[0].pc.description;
outpPrice.innerHTML = laptops[0].pc.price + " kr";
imgLaptop.src = laptops[0].pc.image;


fillDropdown();
fillFeatureList();